﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dentistry.Enums
{
    public enum VisitStatus
    {
        VisitTookPlace = 1,
        VisitNotTookPlace = 2,
        VisitPlaned = 3,
        VisitCanceled = 4
    }
}
