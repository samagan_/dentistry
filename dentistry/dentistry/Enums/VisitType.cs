﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dentistry.Enums
{
    public enum VisitType
    {
        StateVisit = 1,
        
        OfficialVisit = 2,
        
        WorkingVisit = 3,

        NotOfficialVisit = 4
    }
}
