﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dentistry.interfaces
{
    public interface IEntity<T>
    {
        public T Id { get; set; }
    }
}
