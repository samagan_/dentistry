﻿using dentistry.models;
using dentistry.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace dentistry
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                Seed(db);
                ////       var patient = db.Patients
                ////                   .Include(p => p.Id);

                ////   foreach (var p in patient)
                ////{
                ////       Console.WriteLine("*-*-*-*-*-*-*-*-*-*-*-*");
                ////       Console.WriteLine($"Name: {p.FirstName}");
                ////       Console.WriteLine($"LastName: {p.LastName}");
                ////       Console.WriteLine($"Patronymic: {p.Patronymic}");
                ////       Console.WriteLine($"DateOfBirth: {p.DateOfBirth}");
                ////       Console.WriteLine($"Sex: {p.Sex}");
                ////       Console.WriteLine($"DateOfCreateAcc: {p.DateOfCreateAcc}");
                ////       Console.WriteLine($"Organization: {p.Organization}");
                ////}

            }

        }

        public static void Seed(ApplicationDbContext db)
        {
            if (!db.Patients.Any())
            {

                var organization = new Organization()
                {
                    Name = "Samagan Oraganization",
                    Address = "Bishkek 5-mikrorarion",
                    Supervisor = "Samagan"
                };
                db.Organizations.Add(organization);
                db.SaveChanges();

                var patient = new Patient()
                {
                    FirstName = "Samagan",
                    LastName = "Karybekov",
                    Patronymic = "Kubanychbekovich",
                    DateOfBirth = new DateTime(2005, 02, 26),
                    Sex = Enums.Sex.Men,
                    DateOfCreateAcc = DateTime.Now,
                    OrganizationId = organization.Id
                };
              
                db.Patients.Add(patient);
                db.SaveChanges();

                var visit = new Visit()
                {
                    VisitType = Enums.VisitType.OfficialVisit,
                    DateOfVisit = DateTime.Now,
                    VisitStatus = Enums.VisitStatus.VisitPlaned,
                    Price = 15.000m,
                    PatientId = patient.Id
                };
                db.Visits.Add(visit);
                db.SaveChanges();
            }
        }
    }
}
