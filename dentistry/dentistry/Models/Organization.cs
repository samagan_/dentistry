﻿using dentistry.interfaces;
using dentistry.models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dentistry.Models
{
    public class Organization : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Supervisor { get; set; }
        public virtual Patient Patient { get; set; }
    }
}
