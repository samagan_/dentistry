﻿using dentistry.Enums;
using dentistry.interfaces;
using dentistry.models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dentistry.Models
{
    public class Visit : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Patient")]
        public int PatientId { get; set; }

        [Required]
        public VisitType VisitType { get; set; }
        public DateTime DateOfVisit { get; set; }
        public VisitStatus VisitStatus { get; set; }
        public decimal Price { get; set; }
        public virtual Patient Patient { get; set; }
    }
}
