﻿using dentistry.Enums;
using dentistry.interfaces;
using dentistry.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dentistry.models
{
    public class Patient : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Organization")]
        public int OrganizationId { get; set; }
        
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string Patronymic { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime DateOfBirth { get; set; }
        public Sex Sex { get; set; }
        public DateTime DateOfCreateAcc { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
